from sklearn.cluster import KMeans
import numpy as np 

K = 3

def acc(cluster, label_test, labels):
	global confusion
	correct = 0
	for i in range(len(label_test)):
		confusion[int(label_test[i]) - 1][int(cluster[labels[i]]) - 1] += 1
		if(label_test[i] == cluster[labels[i]]):
			correct += 1
	return correct/len(label_test)

data_t = np.genfromtxt("HW3_DataSets/Q1/Train_Data.csv", delimiter=',')
label_t = np.genfromtxt("HW3_DataSets/Q1/Train_Labels.csv", delimiter=',')
data_test = np.genfromtxt("HW3_DataSets/Q1/Test_Data.csv", delimiter=',')
label_test = np.genfromtxt("HW3_DataSets/Q1/Test_Labels.csv", delimiter=',')

kmeans = KMeans(n_clusters=K, random_state=0).fit(data_t)

clusters = []
for i in range(K):
	a = [label_t[j] for j in range(len(kmeans.labels_)) if kmeans.labels_[j] == i]
	clusters.append(max(set(a), key = a.count))

confusion = np.zeros(shape=(K,K))
print(acc(clusters, label_t, kmeans.labels_))
print("confusion:", confusion)