import numpy as np
from numpy import linalg as LA

K = 3
NUMFEAT = 13
NUMITERATION = 100

def distance(a, b):
	return LA.norm(a - b)

def acc(cluster, label_test, labels):
	global confusion
	correct = 0
	for i in range(len(label_test)):
		confusion[int(label_test[i]) - 1][int(cluster[labels[i]]) - 1] += 1
		if(label_test[i] == cluster[labels[i]]):
			correct += 1
	return correct/len(label_test)


data_t = np.genfromtxt("HW3_DataSets/Q1/Train_Data.csv", delimiter=',')
label_t = np.genfromtxt("HW3_DataSets/Q1/Train_Labels.csv", delimiter=',')
data_test = np.genfromtxt("HW3_DataSets/Q1/Test_Data.csv", delimiter=',')
label_test = np.genfromtxt("HW3_DataSets/Q1/Test_Labels.csv", delimiter=',')

centers = []
for i in range(K):
	temp = [0]*NUMFEAT
	for j in range(NUMFEAT):
		temp[j] = float(np.random.rand(1) * (np.max(data_t[:,j]) - np.min(data_t[:,j])) + np.min(data_t[:,j]))
	centers.append(temp)

labels = []
for n in range(NUMITERATION):
	#find each node based on cluster centeroid
	labels = []	
	for i in data_t:
		temp = []
		for j in range(K):
			temp.append(distance(i, centers[j]))
		labels.append(np.argmin(temp))

	#recalculate the centeroid
	for i in range(K):
		a = [data_t[j] for j in range(len(labels)) if labels[j] == i]
		centers[i] = np.mean(a, axis=0)

#check the results

clusters = []
for i in range(K):
	a = [label_t[j] for j in range(len(labels)) if labels[j] == i]
	clusters.append(max(set(a), key = a.count))
print(clusters)

confusion = np.zeros(shape=(K,K))
print(acc(clusters, label_t, labels))
print("confusion:", confusion)