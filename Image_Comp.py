from skimage import io
import numpy as np
from sklearn.cluster import KMeans

K = 2

#  read and show original image
image = io.imread('HW3_DataSets/Q2/landscape.jpg')
io.imshow(image)
io.show()
 
rows, cols = image.shape[0],image.shape[1]
image = image.reshape(rows*cols,3)
# print(image)
# do kmeans (think of order=20 clusters or so!)
kmeans = KMeans(n_clusters=K, random_state=0).fit(image)

b = []
for i in range(K):
	a = [0,0,0]
	for j in range(3):
		a[j] = sum([image[k][j] for k in range(len(image)) if kmeans.labels_[k] == i])/len([image[k][j] for k in range(len(image)) if kmeans.labels_[k] == i])
	b.append(a)

image2 = image
for i in range(len(kmeans.labels_)):
	image2[i] = b[kmeans.labels_[i]]

# print(image2)

# get your clusters and labels
# find clusters and labels 
image2 = image2.reshape(rows,cols,3);  



# # show decompressed image
# image = np.zeros((rows,cols,3),dtype=np.uint8 )
# for i in range(rows):
#     for j in range(cols):
#             image[i,j,:] = clusters[labels[i,j],:]
io.imsave('HW3_DataSets/Q2/compressed_image.png',image2);
io.imshow(image2)
io.show()